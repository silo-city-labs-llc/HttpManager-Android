package com.silocitylabs.httpmanagerexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.silocitylabs.httpmanager.HttpAsync;
import com.silocitylabs.httpmanager.HttpManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        runHttpCall();
    }

    private void runHttpCall() {

        final TextView textView = (TextView)findViewById(R.id.mainTextView);

        HttpManager.ErrorMessageCallback errorMessageCallback = new HttpManager.ErrorMessageCallback() {
            @Override
            public void ErrorMessageCallback(final String errorMessage) {
                runOnUiThread(new Runnable() {
                                  public void run() {
                                      textView.setText("ERROR: "+errorMessage);
                                  }
                              });
            }
        };

        HttpAsync.Interface callback = new HttpAsync.Interface() {
            @Override
            public void HttpManCallback(final String response) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        textView.setText(response);
                    }
                });

            }
        };

        HttpAsync httpAsync = new HttpAsync()
                .setUrlString("http://silocitylabs.com")
                .setActivity(this)
                .setErrorMessageCallback(errorMessageCallback)
                .setCallbackInterface(callback);

        httpAsync.execute();


    }
}
