package com.silocitylabs.httpmanager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class HttpManager {

    private static CookieManager cookieManager;
    private String response = "";
    private int status_code;

    public static final String TAG = "HttpManager";
    private final static String sessionIdName = "PHPSESSID";

    private String urlString = "";
    private List<Pair<String, String>> postData;
    private String postDataString = "";
    private Activity activity;
    private static boolean debugLogging = true;
    private ErrorMessageCallback errorMessageInterface;

    public HttpManager(){ }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public void setPostData(List<Pair<String, String>> postData) {
        this.postData = postData;
    }

    public void setPostDataString(String postDataString) {
        this.postDataString = postDataString;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
    
    public void setDebugLogging(boolean debugLogging) {
        this.debugLogging = debugLogging; 
    }

    public void setErrorMessageInterface(ErrorMessageCallback errorMessageInterface) {
        this.errorMessageInterface = errorMessageInterface;
    }

    public interface ErrorMessageCallback {
        void ErrorMessageCallback(String response);
    }
    private void sendErrorMessage(String errorMessage) {
        if (errorMessageInterface != null) {
            errorMessageInterface.ErrorMessageCallback(errorMessage);
        }
    }

    public void execute() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (!isConnected) {
            if (debugLogging) {
                Log.e(TAG, "No internet connection");
            }
            sendErrorMessage("No internet connection");
            return;
        }

        URL url;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            sendErrorMessage("URL formatting error.");
            return;
        }
        try {
            URLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setDoOutput(true);
            if (conn instanceof HttpsURLConnection) {
                ((HttpsURLConnection) conn).setRequestMethod("POST");
            } else {
                ((HttpURLConnection) conn).setRequestMethod("POST");
            }

            //load previously-saved cookies
            cookieFromPref(activity, urlString);
            if (cookieManager != null) {
                String cookies = cookieStoreToString(cookieManager.getCookieStore());
                conn.setRequestProperty("Cookie", cookies);
            }

            //set POST data
            conn.setRequestProperty("User-Agent", System.getProperty("http.agent"));
            if (activity != null) {
                //display size
                DisplayMetrics display = activity.getResources().getDisplayMetrics();
                int width = display.widthPixels;
                int height = display.heightPixels;
                conn.setRequestProperty("screenW", String.valueOf(width));
                conn.setRequestProperty("screenH", String.valueOf(height));

                //language
                String lang = Locale.getDefault().getLanguage();
                conn.setRequestProperty("lang", lang);

                String lang4 = Locale.getDefault().getVariant();
                if (!lang4.equals("")) {
                    conn.setRequestProperty("lang4", lang + "-" + lang4);
                }

                //Android version
                String androidVersion = "Android " + android.os.Build.VERSION.RELEASE;
                conn.setRequestProperty("os", androidVersion);
            }
            // put post data in the connection
            // manual postDataStrings override any Paired postData
            if (postData != null && (postDataString == null || postDataString.equals(""))) {
                postDataString = createQueryStringForParameters(postData);
            }
            if (postDataString != null && !postDataString.equals("")) {
                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

                //String data = createQueryStringForParameters(postData);
                if (debugLogging) {
                    Log.e(TAG + " post string", postDataString);
                }
                writer.write(postDataString);
                writer.flush();
            }


            //send the HTTP request
            InputStream in = conn.getInputStream();
            if (conn instanceof HttpsURLConnection) {
                status_code = ((HttpsURLConnection) conn).getResponseCode();
            } else {
                status_code = ((HttpURLConnection) conn).getResponseCode();
            }
            response = convertStreamToString(in);
            if (debugLogging) {
                Log.e("httpman response", "" + response);
            }

            //save returned cookies and session
            if (activity != null) {
                cookieManager = new CookieManager();
                final String COOKIES_HEADER = "Set-Cookie";
                Map<String, List<String>> headerFields = conn.getHeaderFields();
                List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

                if (cookiesHeader != null) {
                    for (String cookie : cookiesHeader) {
                        //store the cookies into the local manager
                        HttpCookie httpCookie = HttpCookie.parse(cookie).get(0);
                        cookieManager.getCookieStore().add(null, httpCookie);

                        //save the session_id in (persistent) preferences
                        if (httpCookie.getName().equals(sessionIdName)) {
                            SharedPreferences pref = activity.getPreferences(Context.MODE_PRIVATE);
                            pref.edit().putString(sessionIdName, httpCookie.getValue()).apply();
                        }
                    }
                }

            }

        } catch (UnknownHostException e) {
            if (activity != null) {
                sendErrorMessage("Unknown host error.");
            }
            if (debugLogging) {
                Log.e(TAG, "" + e.getMessage());
            }
            return;
        } catch (FileNotFoundException e) {
            if (activity!=null) {
                sendErrorMessage("404 error.");
                Log.e(TAG, "404. URL: " + urlString + "\nResponse: " + response);
            }
            return;
        } catch (IOException e) {
            if ( debugLogging )
                Log.e(TAG, "HTTP IO exception: " + e.getMessage());
            e.printStackTrace();
            if (activity!=null) {
                if (e.getMessage() != null && e.getMessage().contains("Connection refused"))
                    sendErrorMessage("Connection refused");
                else
                    sendErrorMessage("Connection error.");
            }
            return;
        } catch (Exception e) {
            if ( debugLogging ) {
                Log.e(TAG, "Generic HTTP response error: \npage:" + urlString + " \nmessage:" + e.getMessage());
                if (e.getCause()!=null) {
                    Log.e(TAG, "Generic HTTP response error details:\nCause: "
                            + e.getCause().toString() + "\nDetails: " + e.getLocalizedMessage());
                } else {
                    e.printStackTrace();
                }
            }
            if (activity!=null)
                sendErrorMessage("An unknown error occurred.");
        }

        if (response.equals("")) {
            if ( debugLogging )
                Log.e(TAG, "Blank server response. Check connection. URL: " + urlString);
            if (activity!=null)
                sendErrorMessage("No internet access.");
        }

    }

    public String executeWithResponse() {
        execute();
        return response;
    }
    public int executeWithStatusCode() {
        execute();
        return status_code;
    }


    public static void logout(Activity activity) {
        SharedPreferences pref = activity.getPreferences(Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putString(sessionIdName, null);
        editor.apply();

        setCookieManager(null);

        //TODO: not sure if we need the login manager
        // MainActivity.loginManager.notifyObservers(false);
    }


    /**
     * Creates a string for use with URLConnection's setRequestProperty
     * format: "name1=value1; name2=value2" ... etc
     * @param cookieStore cookie store to convert
     * @return formatted cookies String
     */
    public static String cookieStoreToString(CookieStore cookieStore) {
        final String SET_COOKIE_SEPARATOR="; ";
        String cookieString = "";
        List<HttpCookie> cookies = cookieStore.getCookies();
        Iterator<HttpCookie> cookieIt = cookies.iterator();
        while( cookieIt.hasNext() ) {
            HttpCookie cookie = cookieIt.next();
            if (!cookie.hasExpired()) {
                cookieString += cookie.getName()+"="+cookie.getValue();
                if (cookieIt.hasNext()) {
                    cookieString = cookieString.concat(SET_COOKIE_SEPARATOR);
                }
            }
        }
        return cookieString;
    }

    public static CookieManager getCookieManager() { return cookieManager; }
    public static void deleteCookieManager() { cookieManager = null; }

    private static void setCookieManager(CookieManager newCookieStore) { cookieManager = newCookieStore; }


    public String getResponse() { return response; }
    public int getStatusCode() { return status_code; }

    public static void cookieFromPref(Activity activity, String urlString) {
        SharedPreferences pref = activity.getPreferences(Context.MODE_PRIVATE);
        String session_id = pref.getString(sessionIdName,null);

        if ( getCookieManager()==null && session_id!=null ){
            // Create a local instance of cookieManager
            cookieManager = new CookieManager();
            // Populate cookies
            HttpCookie cookie = new HttpCookie(sessionIdName, session_id);
            cookie.setVersion(0);
            cookie.setDomain("." + getDomainName(urlString));
            cookie.setPath("/");
            cookieManager.getCookieStore().add(null, cookie);

            setCookieManager(cookieManager);
        }
        /* // debugging logs
        else if ( getCookieManager()!=null ) {
                if ( debugLogging ) {
                    Log.e(TAG,"Cookie already exists");
                }
            }
            else if ( session_id==null ) {
                if ( debugLogging ) {
                    Log.e(TAG,"No session_id stored");
                }
            }
        */
    }

    public static String getDomainName(String urlString)  {
        URI uri = null;
        try {
            uri = new URI(urlString);
        } catch (URISyntaxException e) {
            Log.e(TAG, "URI syntax error");
            return "";
        }
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    private static String convertStreamToString(InputStream is) {
        //TODO: handle these Exceptions. Maybe throw them back and let execute() handle it
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            if (debugLogging)
                Log.e(TAG, "IOexception reading stream");
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                if (debugLogging)
                    Log.e(TAG, "IOexception closing stream");
            }
        }
        return sb.toString();
    }

    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';
    public static String createQueryStringForParameters(List<Pair<String, String>> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for ( Pair<String, String> pair : parameters ) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                parametersAsQueryString.append(pair.first)
                        .append(PARAMETER_EQUALS_CHAR)
                        .append(URLEncoder.encode(pair.second));

                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }

}