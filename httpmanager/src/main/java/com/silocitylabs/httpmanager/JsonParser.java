package com.silocitylabs.httpmanager;

import android.app.Activity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

    private static String TAG = "JsonParser";

    /**
     * Parses a string of JSON into a JSONObject.
     * @param jsonString - String given by the server
     * @return - JSONObject form of the given string
     */
    public static JSONObject stringToJsonObj(String jsonString, Activity activity) {
        return stringToJsonObj(jsonString, activity, true);
    }


    /**
     * Parses a string of JSON into a JSONObject.
     * @param jsonString - JSON formatted String object
     * @param activity
     * @param debugLogging - enable/disable errors using Log.e()
     * @return JSONObject form of the given string
     */
    public static JSONObject stringToJsonObj(String jsonString, Activity activity, boolean debugLogging) {
        JSONObject reader = new JSONObject();
        try {

            // attempt recovery if there's HTML in the response (indicating php errors)
            if (!jsonString.startsWith("{")) {
                if (debugLogging)
                    Log.e(TAG, "Error on page. Attempting to fix");
                int jsonIndex = jsonString.indexOf("{");
                if (jsonIndex>=0) {
                    jsonString = jsonString.substring(jsonIndex);
                } else {
                    throw new JSONException("No JSON String found");
                }
            }

            reader = new JSONObject(jsonString);
        } catch (JSONException e) {
            if (debugLogging) {
                Log.e(TAG, "JSON string: " + jsonString);
                Log.e(TAG, "JSON string to JSONObject parse error: " + e.getMessage());
            }
            if (activity!=null) {
                //TODO: ((MainActivity)activity).sendErrorMessage("Error fetching data X_X", "");
            }
        }
        return reader;
    }

    /**
     * Check if a string value is fully empty
     * @param value - String given by the app
     * @return - a boolean
     */
    public static boolean isEmpty(String value) {
        return (value == null || value.isEmpty());
    }


}
