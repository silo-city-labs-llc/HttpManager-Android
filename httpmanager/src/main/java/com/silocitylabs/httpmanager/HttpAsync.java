package com.silocitylabs.httpmanager;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Pair;

import java.util.List;

public class HttpAsync extends AsyncTask<String, Void, String> {

    public interface Interface {
        void HttpManCallback(String response);
    }

    Interface callbackInterface;

    String urlString = "";
    List<Pair<String, String>> postData;
    String postDataString = "";
    Activity activity;
    HttpManager.ErrorMessageCallback errorMessageCallback;

    // testing out the Fluent Pattern
    public HttpAsync setCallbackInterface(Interface callbackInterface) {
        this.callbackInterface = callbackInterface;
        return this;
    }

    public HttpAsync setUrlString(String urlString) {
        this.urlString = urlString;
        return this;
    }

    public HttpAsync setPostData(List<Pair<String, String>> postData) {
        this.postData = postData;
        return this;
    }

    public HttpAsync setPostData(String postDataString) {
        this.postDataString = postDataString;
        return this;
    }

    public HttpAsync setActivity(Activity activity) {
        this.activity = activity;
        return this;
    }

    public HttpAsync setErrorMessageCallback(HttpManager.ErrorMessageCallback callback) {
        this.errorMessageCallback = callback;
        return this;
    }

    public HttpAsync () {}

    @Override
    protected String doInBackground(String... url) {
        HttpManager man = new HttpManager();
        if (urlString!=null) {
            man.setUrlString(urlString);
        }
        if (activity!=null) {
            man.setActivity(activity);
        }
        if (postData!=null) {
            man.setPostData(postData);
        }
        if (postDataString!=null) {
            man.setPostDataString(postDataString);
        }
        if (errorMessageCallback!=null) {
            man.setErrorMessageInterface(errorMessageCallback);
        }
        return man.executeWithResponse();
    }


    protected void onPostExecute(String result) {
        callbackInterface.HttpManCallback(result);
    }
}
